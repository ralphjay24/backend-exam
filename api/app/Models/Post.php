<?php

namespace App\Models;

use App\Models\BaseModel;

class Post extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'slug',
        'user_id',
        'image',
    ];
}

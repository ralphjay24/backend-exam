<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['json.response'], 'namespace' => "App\Http\Controllers"], function() {
    // public routes
    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','Auth\ApiAuthController@register')->name('register.api');

    // Posts
    Route::apiResource('posts', PostController::class)->only(['index', 'show'])->scoped([
        'post' => 'slug',
    ]);;

    Route::middleware('auth:api')->group(function() {
        Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');

        // Posts
        Route::apiResource('posts', PostController::class)->only(['store', 'update', 'destroy'])->scoped([
            'post' => 'slug',
        ]);
    });
});
Laravel Blog Application
========================

A blog application made by Ralph Jay Pepito

## Built With

* [Laravel](https://laravel.com/docs/8.x) - The backend web framework used
* [React](https://reactjs.org/docs/getting-started.html) - The frontend web framework used

Requirements
------------

  * PHP 7.2.9 or higher;
  * MySQL 5.6 or higher;
  * Composer;
  * Node and NPM;

## Build Setup

``` bash
FRONTEND
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm start

BACKEND
$ cd ./api

# install dependencies
$ composer install

# configurize your database first
# follow instructions below on `Database configuration` section
# run migration
$ php artisan migrate

# install laravel passport
$ php artisan passport:install

# run local server at localhost:8000
$ php artisan serve

```

## Database configuration

Create database `blog` using terminal or sql client tool and update database config in `.env` file.

```sh
# /path/to/backend-exam/api/.env
DB_DATABASE=blog
DB_USERNAME=username
DB_PASSWORD=secretpass
```
*Note: You can edit also `DB_HOST` and `DB_PORT` depending on your local sql config.*